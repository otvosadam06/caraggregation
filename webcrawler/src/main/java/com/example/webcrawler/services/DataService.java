package com.example.webcrawler.services;

import com.example.webcrawler.dto.ModelDto;
import com.example.webcrawler.dto.StatisticDto;
import com.example.webcrawler.entities.Car;
import com.example.webcrawler.entities.Data;
import com.example.webcrawler.repositories.DataRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.util.ObjectUtils;

import javax.persistence.criteria.*;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.sql.Date;
import java.util.List;

@Service
public class DataService {
    @Autowired
    DataRepository dataRepository;

    public void setData(String makes) {
        System.out.println(makes);
        if(!ObjectUtils.isEmpty(makes)){
            Data data = new Data();
            data.setMakes(makes);
            data.setDate(Date.valueOf(LocalDate.now()));
            dataRepository.save(data);
        }else{
        }

    }

//    public List<Data> getDatas(Date Start, Date End){
//        return dataRepository.findAll(new Specification<Data>(){
//            @Override
//            public Predicate toPredicate(Root<Data> root, CriteriaQuery<?> cq, CriteriaBuilder cb) {
//                Predicate p = cb.conjunction();
//
//                if(Start != null && End !=null){
//                   // p = cb.and(p, cb.between(root.get("date"), Start, End));
//                    p = cb.and(cb.count(cb.and(p, cb.between(root.get("date"), Start, End))));
//                }
//                return p;
//            }
//        });
//    }
    public StatisticDto[] getDatas(Date start, Date end){
        String[] datas= dataRepository.findByDateBetween(start,end);
        StatisticDto[] statisticDtos = new StatisticDto[datas.length];

        for(int i=0; i<datas.length; i++){
            String data[] = datas[i].split(",");
            statisticDtos[i] = new StatisticDto();
            statisticDtos[i].setMakes(data[0]);
            statisticDtos[i].setSearchNumber(data[1]);
        }
        return statisticDtos;
    }
}
