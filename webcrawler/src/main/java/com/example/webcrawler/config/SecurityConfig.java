package com.example.webcrawler.config;

import com.example.webcrawler.services.JwtService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import javax.sql.DataSource;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

   @Autowired
   DataSource dataSource ;

   @Autowired
   JWTAutheticationConfig jwtAutheticationConfig;

   @Autowired
   JwtRequestFilter jwtRequestFilter;

   @Autowired
   UserDetailsService jwtService;

   @Autowired
   PasswordEncoder passwordEncoder;

   @Value("${spring.queries.users-query}")
   private String userQuery;
   @Value("${spring.queries.roles-query}")
   private  String roleQuery;

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception{
        auth.userDetailsService(jwtService).passwordEncoder(passwordEncoder);
//        auth.jdbcAuthentication()
//                .dataSource(dataSource)
//                .usersByUsernameQuery(
//                        userQuery)
//                .authoritiesByUsernameQuery(
//                        roleQuery);
   }
   @Bean
   @Override
   public AuthenticationManager authenticationManagerBean() throws Exception {
        return  super.authenticationManagerBean();
   }

    @Override
    protected void configure(HttpSecurity http) throws Exception{
        http.cors();
        http.authorizeRequests().antMatchers(HttpMethod.POST,"/register").permitAll();
        http.csrf().disable()
                .authorizeRequests().antMatchers("/authenticate").permitAll()
                .antMatchers(HttpHeaders.ALLOW).permitAll()
                .anyRequest().authenticated()
               .and()
               .exceptionHandling().authenticationEntryPoint(jwtAutheticationConfig)
                .and()
                .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);


                http.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
//                http.authorizeRequests()
//                .antMatchers(HttpMethod.POST, "/register").permitAll()
//                .antMatchers(HttpMethod.GET,"/login").permitAll().
//                antMatchers("/get","/post", "/getData").hasAnyRole("USER", "ADMIN")
//                .antMatchers( "/users", "deleteUser", "updateUser").hasAuthority("ADMIN")
//                .anyRequest().authenticated();
////                .defaultSuccessUrl("http://localhost:4200/home");

    }

}
