package com.example.webcrawler.controller;

import com.example.webcrawler.dto.UserDto;
import com.example.webcrawler.entities.User;
import com.example.webcrawler.repositories.UserRepository;
import com.example.webcrawler.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.web.JsonPath;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.server.reactive.HttpHandler;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.util.ObjectUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.function.EntityResponse;

import java.net.http.HttpResponse;
import java.security.Principal;
import java.util.List;


@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class UserController {
    @Autowired
    UserRepository userRepository;
    @Autowired
    UserService userService;

    @Autowired
    BCryptPasswordEncoder bCryptPasswordEncoder;

    @PostMapping("/register")
    public String register(@RequestBody UserDto userDto){
           return userService.register(userDto);
    }

    @GetMapping("/users")
    @PreAuthorize("hasRole('ADMIN')")
    public List<User> getUsers(){
        return userRepository.findAll();
    }

    @PostMapping("/deleteUser")
    @PreAuthorize("hasRole('ADMIN')")
    public void deleteUser(@RequestBody String login){
         userService.deleteByLogin(login);
    }

    @PostMapping("/updateUser")
    @PreAuthorize("hasRole('ADMIN')")
    public void updateUser(@RequestBody UserDto userDto){
        userService.updateUser(userDto);
    }
}
